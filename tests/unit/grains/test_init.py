import grains.grains.init as grains
import pytest


@pytest.mark.asyncio
class TestGrains:
    async def test_cli(self, mock_hub):
        """
        Test getting option from the target
        """
        mock_hub.OPT = {"grains": {"items": [], "item": [], "out": "json"}}
        grains.cli(mock_hub)

        mock_hub.pop.conf.integrate.assert_called_once()
        mock_hub.grains.init.standalone.assert_called_once_with()

    async def test_standalone(self, mock_hub):
        grains.standalone(mock_hub)
        mock_hub.pop.loop.start.assert_called_once()
        mock_hub.grains.init.collect.assert_called_once()

    async def test_collect(self, mock_hub):
        await grains.collect(mock_hub)

        mock_hub.grains.init.run_sub.assert_called_once_with(mock_hub.grains)
        mock_hub.grains.init.process_subs.assert_called_once()

    async def test_run_sub(self, mock_hub):
        await grains.run_sub(mock_hub, [])

    async def test_process_subs(self, mock_hub):
        await grains.process_subs(mock_hub)
        mock_hub.pop.sub.iter_subs.assert_called_once_with(
            mock_hub.grains, recurse=True
        )
