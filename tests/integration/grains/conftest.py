import os
import pathlib
import pop.hub
import pytest
import sys


@pytest.fixture(scope="function")
async def path() -> pathlib.PosixPath:
    # Relative path to test grains folder
    return pathlib.Path(os.path.dirname(__file__)).parent.parent


@pytest.fixture(scope="function")
async def hub(path) -> pop.hub.Hub:
    # Extend PYTHONPATH with the tests directory so that collect can detect the grains in it.
    sys.path.append(str(path))

    # Create a hub and load the grains sub
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="grains")

    await hub.grains.init.collect()

    yield hub

    del hub.grains.GRAINS
