import asyncio
import json
import os
import pathlib
import pytest
import re
import sys
import yaml
from typing import Any, Dict, Tuple

# 7-bit C1 ANSI sequences
ansi_escape = re.compile(
    r"""
    \x1B  # ESC
    (?:   # 7-bit C1 Fe (except CSI)
        [@-Z\\-_]
    |     # or [ for CSI, followed by a control sequence
        \[
        [0-?]*  # Parameter bytes
        [ -/]*  # Intermediate bytes
        [@-~]   # Final byte
    )
""",
    re.VERBOSE,
)


@pytest.mark.asyncio
class TestGrains:
    @staticmethod
    async def cmd_run(path: pathlib.PosixPath, args: str) -> Tuple[str, str]:
        run = os.path.abspath(os.path.join(path.parent, "run.py"))
        proc = await asyncio.create_subprocess_shell(
            f"{sys.executable} {run} {args}",
            stderr=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
            env={"PYTHONPATH": ":".join([str(path), str(path.parent)])},
        )
        out, err = await asyncio.wait_for(proc.communicate(), timeout=10)
        # Strip color formatting from output
        return ansi_escape.sub("", out.decode()), ansi_escape.sub("", err.decode())

    @staticmethod
    async def load_yaml(data: str) -> Dict[str, Any]:
        return yaml.safe_load(data)

    async def test_collect_order(self, hub):
        """
        Verify that root grains are collected first, then the next level of nested grain
        """
        # If the grains aren't loaded in the expected order there will be a key error in collect
        assert hub.grains.GRAINS["nest"]
        assert hub.grains.GRAINS["nest"]["value"]

    async def test_not_async(self, hub):
        """
        Verify that a grain loaded without an async function was collected
        """
        assert hub.grains.GRAINS["not_async"]

    async def test_skip_underscore(self, hub):
        """
        Verify that a function starting with an underscore was skipped
        """
        assert not hub.grains.GRAINS.get("underscore")

    async def test_cli_all(self, hub, path):
        """
        Verify that the cli returns sorted grains keys
        """
        out, err = await self.cmd_run(path, "--out json")
        assert not err

        result = json.loads(out)
        assert result

        keys = [x for x in result.keys()]
        assert keys

        assert keys == sorted(keys)

    async def test_cli_item(self, hub, path):
        """
        Verify that the cli is capable of returning a specific grain item
        """
        out, err = await self.cmd_run(path, "--item test")
        assert not err

        data = await self.load_yaml(out)
        assert data == {"test": True}

    async def test_cli_item_chain(self, hub, path):
        """
        Verify that the cli is capable of returning a specific grain item
        """
        out, err = await self.cmd_run(path, "-i test -i foo --item bar")
        assert not err

        data = await self.load_yaml(out)
        assert data == {"test": True, "foo": "None", "bar": "None"}

    async def test_cli_items(self, hub, path):
        """
        Verify that the cli is capable of returning multiple grain items
        """
        out, err = await self.cmd_run(path, "--items test foo bar")
        assert not err

        data = await self.load_yaml(out)
        assert data == {"test": True, "foo": "None", "bar": "None"}
