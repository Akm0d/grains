async def load_test(hub):
    hub.grains.GRAINS["test"] = True
    hub.grains.GRAINS["nest"] = {"value": None}


def load_not_async(hub):
    hub.grains.GRAINS["not_async"] = True


def _load_underscore(hub):
    """
    This one should be skipped by the collector
    """

    hub.grains.GRAINS["underscore"] = True
