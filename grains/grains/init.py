# Import python libs
import abc
import asyncio
import collections
import concurrent.futures
import inspect
from typing import Any, Dict, Iterator


class Grains(collections.MutableMapping, abc.ABC):
    """
    An abstract base class that implements the interface of a `dict`
    The difference is that items that are properties will be accessed properly
    items that are async properties will be accessed with an asyncio loop

    Grains that are wrapped with `property` will have their value cached
    """

    def __init__(self, init: Dict[str, Any] = None, *args, **kwargs):
        """
        :param init: A dictionary from which to inherit data
        """
        self._pool = concurrent.futures.ThreadPoolExecutor()
        self._store = dict(*args, **kwargs)
        # A cache for dynamic grains using `property`
        self._cache = {}
        if init:
            # Existing dictionaries might have properties that need wrapped as well
            self.update(init)

    def refresh(self, key: str) -> Any:
        """
        Delete a dynamic grain's cached value and call it's underlying creation function again
        """
        # If the current key wasn't in a cache, but it's subgrains are, then refresh them
        if self._cache.pop(key, None):
            item = self._store.get(key)
            if isinstance(self._store.get(key), Grains):
                item.refresh_all()

        return self.get(key)

    def refresh_all(self) -> Any:
        """
        Destroy the dynamic grains cache so that they will all be regenerated on their next call
        """
        if self._cache:
            del self._cache
            self._cache = {}

    def __setitem__(self, k: str, v):
        """
        Cast all dict values as Grains so that they get the benefits of `property` as well
        """
        if isinstance(v, dict):
            v = Grains(v)
        self._store[k] = v

    def __delitem__(self, k: str):
        if k in self._store:
            del self._store[k]
        if k in self._cache:
            del self._cache[k]

    def __getitem__(self, k: str) -> Any:
        """
        Return the named value from the internal data structures

        If the value is a property, return it's cached value
        If there is no cached value, call the underlying function and return it's value
        """
        item = self._store[k]
        if isinstance(item, property):
            # Return the cached value if it exists, otherwise call the underlying property function
            if k in self._cache:
                return self._cache[k]
            # This is an asynchronous property
            if inspect.iscoroutinefunction(item.fget):
                # TODO this needs to use the existing loop to improve performance
                value = self._pool.submit(asyncio.run, item.fget()).result()
            else:
                # Call the function that returns this property
                value = item.fget()
            self._cache[k] = value
            return value
        return item

    def __len__(self) -> int:
        return len(self._store)

    def __iter__(self) -> Iterator[Any]:
        return iter(self._store)

    def __str__(self):
        return str(self._store)


def __init__(hub):
    # Set up the central location to collect all of the grain data points
    hub.pop.sub.add(dyne_name="output")
    hub.pop.sub.add(dyne_name="exec")
    hub.pop.sub.load_subdirs(hub.grains, recurse=True)
    hub.grains.GRAINS = Grains()


def cli(hub):
    hub.pop.conf.integrate(["grains"], cli="grains", roots=True, loader="yaml")
    hub.grains.init.standalone()

    # Select grains one a time or in groups and print them together
    items = hub.OPT["grains"].get("items")
    outputter = getattr(hub, f"output.{hub.OPT['grains']['out']}.display")
    if items:
        print(outputter({item: hub.grains.GRAINS.get(item) for item in items}))
    else:
        # Print all the grains sorted by dict key
        sorted_keys = sorted(hub.grains.GRAINS.keys(), key=lambda x: x.lower())
        sorted_grains = {key: hub.grains.GRAINS[key] for key in sorted_keys}

        print(outputter(sorted_grains))


def standalone(hub):
    """
    Run the grains sequence in a standalone fashon, useful for projects without
    a loop that want to make a temporary loop or from cli execution
    """
    hub.pop.loop.start(hub.grains.init.collect())


async def collect(hub):
    """
    Collect the grains that are presented by all of the app-merge projects that
    present grains.
    """
    # Remove all cached dynamic grains before collecting everything from scratch
    hub.grains.GRAINS.refresh_all()
    await hub.grains.init.run_sub(hub.grains)
    # Load up the subs with specific grains
    await hub.grains.init.process_subs()


async def run_sub(hub, sub):
    """
    Execute the contents of a specifc sub, all modules in a sub are executed
    in parallel if they are coroutines
    """
    coros = []
    for mod in sub:
        if mod.__sub_name__ == "init":
            continue
        for func in mod:
            ret = func()
            if asyncio.iscoroutine(ret):
                coros.append(ret)
    for fut in asyncio.as_completed(coros):
        await fut


async def process_subs(hub):
    """
    Process all of the nested subs found in hub.grains
    Each discovered sub is hit in lexographical order and all pluigins and functions
    exposed therein are executed in parallel if they are coroutines or as they
    are found if they are natural functions
    """
    for sub in hub.pop.sub.iter_subs(hub.grains, recurse=True):
        await hub.grains.init.run_sub(sub)
