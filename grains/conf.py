CLI_CONFIG = {
    "items": {
        "positional": True,
        "default": [],
        "nargs": "*",
        "help": "Print the named grains",
        "type": str,
    },
    "out": {
        "default": "nested",
        "type": str,
        "help": "The outputter to display the grains",
    },
}
CONFIG = {}
GLOBAL = {}
SUBS = {}
DYNE = {
    "grains": ["grains"],
}
