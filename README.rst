******
GRAINS
******
**System information discovery and asset tracking**

INSTALLATION
============



Clone the `grains` repo and install with pip::

    git clone https://gitlab.com/saltstack/pop/grains.git
    pip install -e grains

EXECUTION
=========
After installation the `grains` command should now be available

Note* Until a vertically app-merged project is also installed
it will have no output

TESTING
=======
install `requirements-test.txt` with pip and run pytest::

    pip install -r grains/requirements-test.txt
    pytest grains/tests

VERTICAL APP-MERGING
====================
Instructions for extending grains for a kernel-specific project

Install pop::

    pip install --upgrade pop

Create a new directory for the project::

    mkdir pop_{kernel}
    cd pop_{kernel}


Use `pop-seed` to generate the structure of a project that extends `grains` and `idem`::

    pop-seed -t v pop_{kernel} -d grains exec states

* "-t v" specifies that this is a vertically app-merged project
*  "-d grains exec states" says that we want to implement the dynamic names of "grains", "exec", and "states"

Add "grains" to the requirements.txt::

    echo "grains @ git+https://gitlab.com/saltstack/pop/grains.git" >> requirements.txt

Note* url based reqs aren't supported on older versions of setuptools
To pip install your vertically app-merged project install grains manually::

    pip install -e git+https://gitlab.com/saltstack/pop/grains.git#egg=chunkies

And that's it!  Go to town making grains, execution modules, and state modules specific to your kernel.
Follow the conventions you see in grains.

For information about running idem states and execution modules check out
https://idem.readthedocs.io
